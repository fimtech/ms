angular.module('starter.controllers', [])

.controller('ResultsCtrl', function($scope, Results) {
    // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.results = Results.all();
  $scope.remove = function(profile) {
    Results.remove(profile);
  };


})


.controller('MapCtrl', function($scope, Chats) {
    // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };


})

.controller('startCtrl', function($scope, $state, $ionicSlideBoxDelegate) {

  $scope.goToTab = function(){
  $state.go('tab.profiles');
  }
 
  // Called to navigate to the main app
  $scope.startApp = function() {
    $state.go('main');
  };
  $scope.next = function() {
    $ionicSlideBoxDelegate.next();
  };
  $scope.previous = function() {
    $ionicSlideBoxDelegate.previous();
  };

  // Called each time the slide changes
  $scope.slideChanged = function(index) {
    $scope.slideIndex = index;
  };
})




.controller('DrivesCtrl', function($scope, Drives) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
 

  $scope.drives = Drives.all();
  $scope.remove = function(drive) {
    Drives.remove(drive);
  };
})



.controller('ProfilesCtrl', function($scope, Profiles) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.SayHello = function() {
    var elem = document.getElementById("myBar"); 
    var width = 1;
    var id = setInterval(frame, 100);
    function frame() {
        if (width >= 100) {
            clearInterval(id);
        } else {
            width++; 
            elem.style.width = width + '%'; 
        }
    }
  var button12 = document.getElementById("myButton");
  button12.innerHTML = "Show Results";
  //button12.removeAttribute("ng-click");
  //button12.setAttribute("ui-sref", "tab.results");
  console.log(button12.value)
  $scope.registered = true;
  $scope.registered2 = false;


  
  }
  $scope.registered2 = true;

 

  $scope.profiles = Profiles.all();
  $scope.remove = function(profile) {
    Profiles.remove(profile);
  };
})

.controller('ProfileDetailCtrl', function($scope, $stateParams, Profiles) {
  $scope.profile = Profiles.get($stateParams.profileId);


})

.controller('LoadingCtrl', ['$scope', '$state', '$timeout',
                                function($scope, $state, $timeout) {

console.log("fbkhs")
    $timeout(function() {
      $state.go('start');
      }, 3000);

    }])



.controller('MapCtrl2', function($scope, $cordovaGeolocation, $ionicLoading, Chats) {
  $scope.chats = Chats.all();
   $scope.$on( "$ionicView.enter", function( scopes, states ) {
           google.maps.event.trigger( map, 'resize' );

        });

  var geocoder;
  var addresses = Chats.all();
     
    ionic.Platform.ready(function(){
        geocoder = new google.maps.Geocoder(); 

        var posOptions = {
            enableHighAccuracy: true,
            timeout: 20000,
        };

        var mapOptions = {
            center: "Augsburg",
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        }; 

        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        $scope.map = map;


        //Wait until the map is loaded
        google.maps.event.addListenerOnce($scope.map, 'idle', function(){              

            for (var i = 0; i < addresses.length ; i++) {
            var pos = new google.maps.LatLng(addresses[i].lat, addresses[i].long);
            var marker = new google.maps.Marker({
                map: $scope.map,
                animation: google.maps.Animation.DROP,
                position: pos,
                
            }); 

            var k = i+1 
            var content = "<a href=#/tab/chats/"+k+">"+addresses[i].location+"</a>";
            var infowindow = new google.maps.InfoWindow()
            google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
            return function() {
            infowindow.setContent(content);
            infowindow.open(map,marker);
            };   
            })(marker,content,infowindow)); 

          }           
        })

        google.maps.event.addListenerOnce($scope.map, 'idle', function(){  
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;             
            var myLatlng = new google.maps.LatLng(lat, long);
            $scope.map.setCenter(myLatlng);
            $scope.map.setZoom(8);
      })
    })
  })               
})

