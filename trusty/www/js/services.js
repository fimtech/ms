angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var areas = [
  {
    "image": "https://i.imgsafe.org/1ddccefdca.png",
    "distance": "214 km",
    "time": "2:20h",
    "name": "Florian Nagel"
  },
  {
    "image": "https://i.imgsafe.org/1ddecb9eb2.png",
    "distance": "23 km",
    "time": "0:40h",
    "name": "Ulrich Buhl"
  },
  {
    "image": "https://i.imgsafe.org/1ddee08898.png",
    "distance": "36 km",
    "time": "1:00h",
    "name": "Kevin Nix"
  },
  {
    "image": "https://i.imgsafe.org/1ddf1beaa7.png",
    "distance": "56 km",
    "time": "0:60h",
    "name": "Kainer Abel"
  },
  {
    "image": "https://i.imgsafe.org/1ddf496e0f.png",
    "distance": "62 km",
    "time": "0:40h",
    "name": "Julius Casear"
  },
  {
    "image": "https://i.imgsafe.org/1ddf5c6336.png",
    "distance": "42 km",
    "time": "0:20h",
    "name": "Tom Halm"
  },
  {
    "image": "https://i.imgsafe.org/1ddf4c0073.png",
    "distance": "2 km",
    "time": "0:05h",
    "name": "Rudi Freud"
  },
  {
    "image": "https://i.imgsafe.org/1ddf40768f.png",
    "distance": "222 km",
    "time": "3:20h",
    "name": "Katrin Theiss"
  }
];

  return {
    all: function() {
      return areas;
    },
    remove: function(area) {
      areas.splice(areas.indexOf(area), 1);
    },
    get: function(areaId) {
      for (var i = 0; i < areas.length; i++) {
        if (areas[i].id === parseInt(areaId)) {
          return areas[i];
        }
      }
      return null;
    }
  };
})


.factory('Profiles', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var profiles = [
  {
    "": 13661,
    "id": 1549975,
    "Name": "Christoph Streit",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/c_4_5_6fd30299b_385620_8/christoph-streit-foto.256x256.jpg",
    "begin_date": "NULL",
    "Titel": "Technology Evangelist & Co-Founder",
    "Unternehmen": "ScaleUp Technologies GmbH & Co. KG",
    "industry": "INFORMATION_TECHNOLOGY_AND_SERVICES",
    "number_of_contacts": 610,
    "common_contacts": 118,
    "degrees_of_sep": 3,
    "lat": 48.143765,
    "lon": 11.5327821,
    "dScore": 4.4
  },
  {
    "": 7938,
    "id": 117943,
    "Name": "Andre Mundo",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/1_e_c_fa395a931_3202815_5/andre-mundo-foto.256x256.jpg",
    "begin_date": "2015-01-01",
    "Titel": "Bereichsleiter Digital Transformation Consulting",
    "Unternehmen": "MaibornWolff GmbH",
    "industry": "INFORMATION_TECHNOLOGY_AND_SERVICES",
    "number_of_contacts": 830,
    "common_contacts": 17,
    "degrees_of_sep": 2,
    "lat": 48.088883,
    "lon": 11.5850657,
    "dScore": 8.5
  },
  {
    "": 3953,
    "id": 4403,
    "Name": "Stephan Hacker",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/7/3/8/cd84cffa6.15946900,4.256x256.jpg",
    "begin_date": "2014-08-01",
    "Titel": "Sportmanagement / Sportvermarktung / Eventmanagement",
    "Unternehmen": "Stephan Hacker - Events & Sportvermarktung",
    "industry": "SPORTS",
    "number_of_contacts": 802,
    "common_contacts": 199,
    "degrees_of_sep": 2,
    "lat": 48.1650422,
    "lon": 11.5781445,
    "dScore": 6.6
  },
  {
    "": 26667,
    "id": 2260415,
    "Name": "Samir Taki",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/0/e/5/126def19c.3656303,1.256x256.jpg",
    "begin_date": "2013-05-01",
    "Titel": "Bereichsleiter IT- und Prozessmanagement",
    "Unternehmen": "norisbank GmbH",
    "industry": "BANKING",
    "number_of_contacts": 170,
    "common_contacts": 114,
    "degrees_of_sep": 3,
    "lat": 48.1729554,
    "lon": 11.5890611,
    "dScore": 6.2
  },
  {
    "": 9901,
    "id": 235672,
    "Name": "Alexandra Maria Fierek",
    "gender": "f",
    "photo_url": "https://www.xing.com/image/f_d_9_eddf0149f_6134585_10/alexandra-maria-fierek-foto.256x256.jpg",
    "begin_date": "2008-01-01",
    "Titel": "Senior Account Manager",
    "Unternehmen": "goFLUENT GmbH -  Business Language Training (eLearning, VCR Training, etc.)",
    "industry": "ELEARNING",
    "number_of_contacts": 88,
    "common_contacts": 15,
    "degrees_of_sep": 2,
    "lat": 48.114507,
    "lon": 11.5860916,
    "dScore": 7.4
  },
  {
    "": 12401,
    "id": 383245,
    "Name": "Rainer Backes",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/d_c_d_42e2e061d_7401055_2/rainer-backes-foto.256x256.jpg",
    "begin_date": "2014-01-01",
    "Titel": "Geschäftsführer",
    "Unternehmen": "gbs - Gesellschaft für Banksysteme GmbH",
    "industry": "INFORMATION_TECHNOLOGY_AND_SERVICES",
    "number_of_contacts": 640,
    "common_contacts": 202,
    "degrees_of_sep": 2,
    "lat": 48.1329226,
    "lon": 11.5473293,
    "dScore": 0.7
  },
  {
    "": 14658,
    "id": 1771866,
    "Name": "Torsten Elsner",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/4_d_4_ca65bd79b_3418309_3/torsten-elsner-foto.256x256.jpg",
    "begin_date": "NULL",
    "Titel": "Redaktion",
    "Unternehmen": "Liga3-Online",
    "industry": "JOURNALISM",
    "number_of_contacts": 376,
    "common_contacts": 196,
    "degrees_of_sep": 3,
    "lat": 48.1507438,
    "lon": 11.5518084,
    "dScore": 1.3
  },
  {
    "": 14710,
    "id": 1786083,
    "Name": "Adrian Betz",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/0/f/f/d2a4c8482.3662933,1.256x256.jpg",
    "begin_date": "NULL",
    "Titel": "Project Manager",
    "Unternehmen": "Siemens AG, Energy Sector",
    "industry": "HUMAN_RESOURCES",
    "number_of_contacts": 729,
    "common_contacts": 207,
    "degrees_of_sep": 3,
    "lat": 48.1319929,
    "lon": 11.5680431,
    "dScore": 4.6
  },
  {
    "": 899,
    "id": 1041,
    "Name": "Javier Ortega Aranegui",
    "gender": "m",
    "photo_url": "https://www.xing.com/assets/frontend_minified/img/users/nobody_m.256x256.jpg",
    "begin_date": "NULL",
    "Titel": "Director de Compras",
    "Unternehmen": "UNIDAD EDITORIAL",
    "industry": "PUBLISHING",
    "number_of_contacts": 521,
    "common_contacts": 213,
    "degrees_of_sep": 2,
    "lat": 48.1709213,
    "lon": 11.563016,
    "dScore": 7.1
  },
  {
    "": 28012,
    "id": 2261889,
    "Name": "Jan Geisler",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/f/c/4/60510239b.6916927,2.256x256.jpg",
    "begin_date": "NULL",
    "Titel": "Director Sponsorship Sales",
    "Unternehmen": "LeadCon Leaders Contact International GmbH / business factors Deutschland GmbH",
    "industry": "OTHERS",
    "number_of_contacts": 807,
    "common_contacts": 195,
    "degrees_of_sep": 3,
    "lat": 48.1355739,
    "lon": 11.5966319,
    "dScore": 3.7
  },
  {
    "": 12410,
    "id": 384804,
    "Name": "Holger Ideler",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/8/7/0/5e9226f14.3194136,6.256x256.jpg",
    "begin_date": "2005-06-01",
    "Titel": "Senior Consultant / Business Analyst / IT- Business Delivery / Freiberufler",
    "Unternehmen": "hiprojecting",
    "industry": "BANKING",
    "number_of_contacts": 528,
    "common_contacts": 106,
    "degrees_of_sep": 2,
    "lat": 48.1761519,
    "lon": 11.5789527,
    "dScore": 5.3
  },
  {
    "": 1823,
    "id": 2139,
    "Name": "Hartwig Ostermeyer",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/c_d_6_9607a548f_3534110_11/hartwig-ostermeyer-foto.256x256.jpg",
    "begin_date": "2015-08-01",
    "Titel": "Executive Manager Corporate Development",
    "Unternehmen": "Sixt SE",
    "industry": "LEISURE_TRAVEL_AND_TOURISM",
    "number_of_contacts": 652,
    "common_contacts": 36,
    "degrees_of_sep": 2,
    "lat": 48.0905687,
    "lon": 11.556999,
    "dScore": 7.5
  },
  {
    "": 17994,
    "id": 2250540,
    "Name": "Stefan Mesch",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/9/d/c/70391da60.16991931,2.256x256.jpg",
    "begin_date": "2016-05-01",
    "Titel": "Consultant",
    "Unternehmen": "ibi research an der Universität Regensburg GmbH",
    "industry": "RESEARCH",
    "number_of_contacts": 390,
    "common_contacts": 104,
    "degrees_of_sep": 3,
    "lat": 48.1362745,
    "lon": 11.5400178,
    "dScore": 1.2
  },
  {
    "": 23787,
    "id": 2257232,
    "Name": "Dipl.-Ing. Marius Totter",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/9_8_d_1d8322bb5_21937451_2/marius-totter-foto.256x256.jpg",
    "begin_date": "2015-06-01",
    "Titel": "Data Analyst",
    "Unternehmen": "Billa AG",
    "industry": "RETAIL",
    "number_of_contacts": 595,
    "common_contacts": 217,
    "degrees_of_sep": 3,
    "lat": 48.1257619,
    "lon": 11.5847302,
    "dScore": 4.4
  },
  {
    "": 25844,
    "id": 2259522,
    "Name": "Holger Kirchner",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/0_b_9_88dd1cf95_7870550_5/holger-kirchner-foto.256x256.jpg",
    "begin_date": "2004-02-01",
    "Titel": "Mediaberater Rhein-Neckar-Pfalz / RPR1. bigFM",
    "Unternehmen": "RPR Unternehmensgruppe",
    "industry": "MOTION_PICTURES",
    "number_of_contacts": 805,
    "common_contacts": 56,
    "degrees_of_sep": 3,
    "lat": 48.1122047,
    "lon": 11.6152865,
    "dScore": 7.4
  },
  {
    "": 7475,
    "id": 101888,
    "Name": "Alexander Link",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/e_7_b_3c2e861cf_18728573_2/alexander-link-foto.256x256.jpg",
    "begin_date": "2016-10-01",
    "Titel": "Aerodynamiker",
    "Unternehmen": "Mercedes-AMG GmbH",
    "industry": "AUTOMOTIVE",
    "number_of_contacts": 372,
    "common_contacts": 140,
    "degrees_of_sep": 2,
    "lat": 48.1312414,
    "lon": 11.6184875,
    "dScore": 2.8
  },
  {
    "": 12896,
    "id": 446867,
    "Name": "Daniel Siebenweiber",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/0/e/c/67503dc98.18894711,5.256x256.jpg",
    "begin_date": "2016-06-01",
    "Titel": "Consultant",
    "Unternehmen": "Mücke, Sturm & Company GmbH",
    "industry": "CONSULTING",
    "number_of_contacts": 882,
    "common_contacts": 129,
    "degrees_of_sep": 3,
    "lat": 48.1285597,
    "lon": 11.629696,
    "dScore": 6.4
  },
  {
    "": 16950,
    "id": 2249336,
    "Name": "Max Wüstehube",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/1_c_9_0f12c041b_20433648_1/max-w%C3%BCstehube-foto.256x256.jpg",
    "begin_date": "2016-07-01",
    "Titel": "Software-Entwickler",
    "Unternehmen": "BSI Business Systems Integration Deutschland GmbH",
    "industry": "INFORMATION_TECHNOLOGY_AND_SERVICES",
    "number_of_contacts": 690,
    "common_contacts": 58,
    "degrees_of_sep": 3,
    "lat": 48.1839764,
    "lon": 11.5598689,
    "dScore": 5.8
  },
  {
    "": 18293,
    "id": 2250898,
    "Name": "Juergen Lang",
    "gender": "m",
    "photo_url": "https://www.xing.com/assets/frontend_minified/img/users/nobody_m.256x256.jpg",
    "begin_date": "NULL",
    "Titel": "Softwareentwickler",
    "Unternehmen": "Allianz",
    "industry": "BANKING",
    "number_of_contacts": 200,
    "common_contacts": 60,
    "degrees_of_sep": 3,
    "lat": 48.1631998,
    "lon": 11.6299136,
    "dScore": 0.9
  },
  {
    "": 28235,
    "id": 2262121,
    "Name": "Tobias Bruch",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/7/f/0/6aeacd32b.6647658,4.256x256.jpg",
    "begin_date": "2014-06-01",
    "Titel": "Expert Account Manager / Teamleiter",
    "Unternehmen": "Brunel GmbH",
    "industry": "STAFFING_AND_RECRUITING",
    "number_of_contacts": 473,
    "common_contacts": 27,
    "degrees_of_sep": 3,
    "lat": 48.1793406,
    "lon": 11.6264531,
    "dScore": 1.4
  },
  {
    "": 18440,
    "id": 2251075,
    "Name": "Dr. Bo Höge",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/5/2/2/be4191066.5889224,3.256x256.jpg",
    "begin_date": "2014-05-01",
    "Titel": "Teamleiter Consulting",
    "Unternehmen": "Xtronic GmbH",
    "industry": "AUTOMOTIVE",
    "number_of_contacts": 308,
    "common_contacts": 101,
    "degrees_of_sep": 3,
    "lat": 48.1618649,
    "lon": 11.6276482,
    "dScore": 1.8
  },
  {
    "": 21209,
    "id": 2254302,
    "Name": "Michael Pollner",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/2_e_a_65df56f37_8840119_5/michael-pollner-foto.256x256.jpg",
    "begin_date": "2015-09-01",
    "Titel": "Business & Innovation Triver",
    "Unternehmen": "trive.me",
    "industry": "COMPUTER_SOFTWARE",
    "number_of_contacts": 426,
    "common_contacts": 134,
    "degrees_of_sep": 3,
    "lat": 48.1095681,
    "lon": 11.5517747,
    "dScore": 8.5
  },
  {
    "": 14650,
    "id": 1769884,
    "Name": "Sophie Troukens",
    "gender": "f",
    "photo_url": "https://www.xing.com/image/7_6_b_39637e87f_15583183_4/sophie-troukens-foto.256x256.jpg",
    "begin_date": "2014-12-01",
    "Titel": "WEB & PRINT DESIGN            ***** 15% Rabatt für Neu-Gründer *****",
    "Unternehmen": "Vlinder Design - Sophie Troukens",
    "industry": "DESIGN",
    "number_of_contacts": 237,
    "common_contacts": 140,
    "degrees_of_sep": 3,
    "lat": 48.1767091,
    "lon": 11.5963489,
    "dScore": 9.5
  },
  {
    "": 2078,
    "id": 2429,
    "Name": "Dominik Hitzler",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/6/5/0/361719f14.6622684,9.256x256.jpg",
    "begin_date": "NULL",
    "Titel": "Musikschaffender | Musikmanager | Musiker",
    "Unternehmen": "Boxberg-Studio GbR",
    "industry": "MUSIC",
    "number_of_contacts": 685,
    "common_contacts": 95,
    "degrees_of_sep": 2,
    "lat": 48.0992544,
    "lon": 11.6204887,
    "dScore": 5
  },
  {
    "": 18182,
    "id": 2250759,
    "Name": "Susanne Reinert",
    "gender": "f",
    "photo_url": "https://www.xing.com/img/users/1/1/d/5540c8852.5695759,6.256x256.jpg",
    "begin_date": "2013-10-01",
    "Titel": "Manager",
    "Unternehmen": "KPMG AG WPG Audit Financial Services",
    "industry": "TAX_ACCOUNTANCY_AUDITING",
    "number_of_contacts": 473,
    "common_contacts": 62,
    "degrees_of_sep": 3,
    "lat": 48.1057208,
    "lon": 11.5521238,
    "dScore": 8.7
  },
  {
    "": 11605,
    "id": 328512,
    "Name": "Johann Wiesböck",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/3/2/d/47b5be329.7566474,3.256x256.jpg",
    "begin_date": "NULL",
    "Titel": "Chefredakteur, Publisher",
    "Unternehmen": "Vogel Business Medien",
    "industry": "PUBLISHING",
    "number_of_contacts": 672,
    "common_contacts": 127,
    "degrees_of_sep": 2,
    "lat": 48.1505288,
    "lon": 11.6109995,
    "dScore": 1.1
  },
  {
    "": 9024,
    "id": 176672,
    "Name": "Andreea Streza",
    "gender": "f",
    "photo_url": "https://www.xing.com/image/2_f_1_10f96c75e_16605332_12/andreea-streza-foto.256x256.jpg",
    "begin_date": "2015-04-01",
    "Titel": "angehende Steuerberaterin",
    "Unternehmen": "Steuerkanzlei",
    "industry": "TAX_ACCOUNTANCY_AUDITING",
    "number_of_contacts": 362,
    "common_contacts": 39,
    "degrees_of_sep": 2,
    "lat": 48.1338427,
    "lon": 11.602366,
    "dScore": 0.1
  },
  {
    "": 1580,
    "id": 1864,
    "Name": "Ali Altun",
    "gender": "m",
    "photo_url": "https://www.xing.com/image/0_f_2_e13ff4e71_16688809_1/ali-altun-foto.256x256.jpg",
    "begin_date": "2016-02-01",
    "Titel": "Auslandspraktikum - Einkauf Interior",
    "Unternehmen": "Daimler Trucks North America",
    "industry": "AUTOMOTIVE",
    "number_of_contacts": 939,
    "common_contacts": 202,
    "degrees_of_sep": 2,
    "lat": 48.1316174,
    "lon": 11.6003653,
    "dScore": 9.5
  },
  {
    "": 10267,
    "id": 265456,
    "Name": "Stephanie Morcinek",
    "gender": "f",
    "photo_url": "https://www.xing.com/img/users/d/7/0/915996585.6336534,10.256x256.jpg",
    "begin_date": "2014-02-01",
    "Titel": "Moderedakteurin",
    "Unternehmen": "GRAZIA",
    "industry": "PUBLISHING",
    "number_of_contacts": 627,
    "common_contacts": 82,
    "degrees_of_sep": 2,
    "lat": 48.1535245,
    "lon": 11.6027333,
    "dScore": 1.6
  },
  {
    "": 18335,
    "id": 2250948,
    "Name": "Christopher Streit",
    "gender": "m",
    "photo_url": "https://www.xing.com/img/users/3/6/e/7b1ed4ba3.22302072,1.256x256.jpg",
    "begin_date": "2014-12-01",
    "Titel": "Werkstudent",
    "Unternehmen": "Rücker + Schindele Beratende Ingenieure",
    "industry": "CONSULTING",
    "number_of_contacts": 806,
    "common_contacts": 4,
    "degrees_of_sep": 3,
    "lat": 48.1749492,
    "lon": 11.5799492,
    "dScore": 4
  }
];

  return {
    all: function() {
      return profiles;
    },
    remove: function(profile) {
      profiles.splice(profiles.indexOf(profile), 1);
    },
    get: function(profileId) {
      for (var i = 0; i < profiles.length; i++) {
        if (profiles[i].id === parseInt(profileId)) {
          return profiles[i];
        }
      }
      return null;
    }
  };
})



.factory('Results', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var results = [{
    id: 0,
    name: 'Benjamin Sparrow',
    lastText: 'You on your way?',
    picture: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    picture: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    picture: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    picture: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    picture: 'img/mike.png'
  }];

  return {
    all: function() {
      return results;
    },
    remove: function(result) {
      results.splice(results.indexOf(result), 1);
    },
    get: function(resultId) {
      for (var i = 0; i < results.length; i++) {
        if (results[i].id === parseInt(resultId)) {
          return results[i];
        }
      }
      return null;
    }
  };
});
